<?php
namespace App\Http\Controllers\Acquiring;


use App\City;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class SberbankPayment implements SberbankPaymentInterface
{
    const TEST_URL = 'https://3dsec.sberbank.ru';
    const PROD_URL = 'https://securepayments.sberbank.ru';

    /**
     * Логин в системе платежного шлюза
     * @var string
     */
    protected $login = '{login}';

    /**
     * Пароль в системе платежного шлюза
     * @var string
     */
    protected $password = '{password}';

    protected $http_client;
    protected $host = self::PROD_URL;
    protected $cityData;

    public function __construct($sberbank_login, $sberbank_password, $acquiring_checking)
    {
        $this->http_client = new Client();

        if (!is_null($sberbank_login)) {
            $this->login = $sberbank_login;
        }

        if (!is_null($sberbank_password)) {
            $this->password = $sberbank_password;
        }

        if ($acquiring_checking == 1) {
            $this->host = self::TEST_URL;
        }

        // Получаем данные о городе
        $this->cityData = City::getActualCityData();
    }

    /**
     * @param int $orderId
     * @param string $description
     * @param int $amount
     * @return bool|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function registerOrder(int $orderId, string $description, int $amount)
    {
        $requestUrl = $this->host . '/payment/rest/register.do';

        $response = $this->http_client->request('GET', $requestUrl, [
            'stream' => true,
            'connect_timeout' => 10,
            'read_timeout' => 10,
            'query' => [
                'userName' => $this->login,
                'password' => $this->password,
                'amount' => $amount,
                'orderNumber' => $orderId,
                'returnUrl' => 'https://' . $this->cityData->slug . '.kolokol.school/payment-done',
                'failUrl' => 'https://' . $this->cityData->slug . '.kolokol.school/payment-false',
            ]
        ]);

        $result = json_decode($response->getBody()->getContents());

        if (isset($result->errorCode)) {
            Log::error('Ошибка эквайринга', [$result->errorCode, $result->errorMessage]);
            return false;
        }

        return $result;
    }

    /**
     * @param string $orderId
     * @return bool|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStatusOrder(string $orderId)
    {
        $requestUrl = $this->host . '/payment/rest/getOrderStatus.do';

        $response = $this->http_client->request('GET', $requestUrl, [
            'stream' => true,
            'connect_timeout' => 10,
            'read_timeout' => 10,
            'query' => [
                'userName' => $this->login,
                'password' => $this->password,
                'orderId' => $orderId,
            ]
        ]);

        return json_decode($response->getBody()->getContents(), JSON_UNESCAPED_UNICODE);
    }
}