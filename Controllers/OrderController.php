<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Controllers\Acquiring\SberbankPaymentInterface;
use App\Http\Request\OrderRequest;
use App\Lesson;
use App\Mail\MailInterface;
use App\Order;
use App\Program;
use App\Repositories\OrderRepositoryInterface;
use App\Services\AmoCRM\AmoServiceInterface;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\AmoCRM;

class OrderController extends Controller
{
    protected $paymentService;
    protected $mailService;
    protected $amoService;
    protected $orderRepository;
    protected $response = [];

    public function __construct(
        SberbankPaymentInterface $paymentService,
        MailInterface $mailService,
        AmoServiceInterface $amoService,
        OrderRepositoryInterface $orderRepository
    )
    {
        $this->paymentService   = $paymentService;
        $this->mailService      = $mailService;
        $this->amoService       = $amoService;
        $this->orderRepository  = $orderRepository;

        parent::__construct();
    }

    /**
     * Обработка и сохранение заказа в БД
     *
     * @param OrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(OrderRequest $request)
    {
        // Сохраняем прошедшие валидацию данные в БД
        $order = $this->orderRepository->store($request->validated());

        // В зависимости от формы, отправляем на оплату и/или бронируем
        $response = $this->processing_by_source($order);

        // Отсылаем данные в необходимые системы учета и информирования
        $this->informingUsersAboutOrder($order);

        // Отправляем ответ на фронт
        return response()->json($response, 201, [], JSON_UNESCAPED_UNICODE);
    }

    public function processing_by_source(Order $order)
    {
            if($order->source == Order::FORM_WITH_PAYMENT) { // Если бронирование через форму оплаты
                return $this->bookingWithPayment($order);
            } elseif ($order->source == Order::SOURCE_SCHEDULE_CERTIFICATE) { // Если бронирование через сертификат
                return $this->bookingWithoutPayment($order);
            }
    }

    public function bookingWithPayment(Order $order)
    {
        // Отправляем данные в эквайринг
        $result = $this->passDataToPayment($order);

        if($result !== false) {
            // Собираем данные для ответа
            $this->response = ['formUrl' => $result->formUrl, 'orderId' => $result->orderId];
        }

        return $this->response;
    }

    public function bookingWithoutPayment(Order $order)
    {
        // Уменьшаем количество свободных мест МК на единицу
        Lesson::reduceEmptySeatsByOne($order);

        return $this->response;
    }

    /**
     * @param Order $order
     */
    private function informingUsersAboutOrder(Order $order): void
    {
        // собираем данные и отправаляем на почту
        $this->mailService->send($order);

        // собираем данные и отправляем в amoCRM
        $this->amoService->send($order);
    }

    /**
     * @param Order $order
     * @return bool|\Illuminate\Http\JsonResponse|mixed
     */
    public function passDataToPayment(Order $order)
    {
        try {
            $result = $this->paymentService->registerOrder(rand(35, 2500) . $order->id, $order->description, $order->sum);

            if($result != false) {
                $order->payment_order_id = $result->orderId;
                $order->save();
            }

            return $result;
        } catch (\Exception $e) {
            Log::error('payment MK' . $e->getMessage());
            return false;
        }
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function done(Request $request)
    {
        try {
            // Получаем статус платежа
            $result = $this->paymentService->getStatusOrder($request['orderId']);

            if ($result['OrderStatus'] === 2) {
                $order = $this->orderRepository->getOrderByPaymentOrderId($request['orderId']);

                if (is_null($order)) {
                    throw new \Exception('Order does not found');
                }

                //Обновляем платежную информацию
                $this->orderRepository->updateOrderPaymentInfo($order, $result);

                //Создаем клиента, после успешной оплаты
                Client::createAndStoreClient($order);

                // Уменьшаем количество свободных мест МК на единицу
                Lesson::reduceEmptySeatsByOne($order);

                // Готовим и отдаем ответ
                $data = $this->buildResponse('оплата прошла успешно', 'оплата');
                return view('after_payment', $data);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function false()
    {
        // Готовим и отдаем ответ
        $data = $this->buildResponse('оплата не прошла', 'неудачная оплата');
        return view('after_bad_payment', $data);
    }

    private function buildResponse(string $title, string $breadcrumbsPage): array
    {
        $data = [
            'title' => $title,
            'breadcrumbs' => [
                '/' => 'Главная',
                '#' => $breadcrumbsPage,
            ],
            'navigation' => $this->schedule,
            'city' => $this->cityName,
        ];

        return array_merge($data, $this->requisites);
    }
}