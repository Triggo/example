<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App
 *
 * @property int $id
 * @property string $user_name
 * @property string $user_phone
 * @property string $user_email
 * @property int $source
 * @property int $activity_status
 * @property int $program_id
 * @property int $landing_id
 * @property int $city_id
 * @property int $paid
 * @property float $sum
 * @property int $payment_status
 * @property string $payment_order_id
 * @property int $payment_error_code
 * @property string $payment_error_message
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property int $lesson_id
 */
class Order extends Model
{
    const SOURCE_PROGRAM = 1;
    const SOURCE_SCHEDULE = 2;
    const SOURCE_LANDING = 3;
    const SOURCE_SCHEDULE_CERTIFICATE = 4;
    const SOURCE_PROGRAM_CERTIFICATE = 5;
    const SOURCE_CERTIFICATE = 6;

    const FORM_WITH_PAYMENT = 2;

    protected $fillable = [
        'user_name',
        'user_phone',
        'user_email',
        'source',
        'lesson_id',
        'program_id',
        'landing_id',
        'city_id',
        'certificate',
        'paid',
        'sum',
        'is_sale',
        'description'
    ];
}
