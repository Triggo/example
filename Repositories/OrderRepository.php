<?php
namespace App\Repositories;

use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderRepository implements OrderRepositoryInterface
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getAllByAuthUserCity()
    {
        return $this->order->where('city_id', Auth::user()->city_id)->orderBy('id', 'desc')->get();
    }

    public function store(array $validatedData)
    {
        return $this->order->create($validatedData);
    }

    public function getOrderByPaymentOrderId(string $orderId)
    {
        return $this->order->where('payment_order_id', $orderId)->first();
    }

    public function updateOrderPaymentInfo(Order $order, array $result)
    {
        $order->payment_status = $result['OrderStatus'];
        $order->payment_error_code = $result['ErrorCode'];
        $order->payment_error_message = $result['ErrorMessage'];
        $order->paid = true;
        $order->save();

        return true;
    }
}