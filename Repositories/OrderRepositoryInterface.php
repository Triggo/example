<?php

namespace App\Repositories;

use App\Order;

interface OrderRepositoryInterface
{
    public function store(array $validatedData);
    public function getAllByAuthUserCity();
    public function getOrderByPaymentOrderId(string $orderId);
    public function updateOrderPaymentInfo(Order $order, array $result);
}