<?php

namespace App\Http\Request;


use App\Order;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name'  => 'string|nullable|max:30',
            'user_phone' => 'integer|nullable',
            'user_email' => 'email|nullable',
            'source'     => 'required|integer|nullable',
            'lesson_id'  => 'integer|nullable',
            'program_id' => 'integer|nullable',
            'landing_id' => 'integer|nullable',
            'city_id'    => 'integer',
            'paid'       => 'integer',
            'sum'        => 'numeric|nullable',
            'certificate'=> 'string|nullable',
            'is_sale'    => 'integer|nullable',
            'description'=> 'string|nullable',
        ];
    }

    public function getValidatorInstance()
    {
        $this->cleanPhoneNumber();
        $this->formatSum();
        $this->checkIsSale();
        $this->orderDescription();
        $this->orderSource();

        return parent::getValidatorInstance();
    }

    protected function cleanPhoneNumber()
    {
        if($this->request->has('user_phone')){
            $this->merge([
                'user_phone' => str_replace(['+', '-', '(', ')', ' '], '', $this->request->get('user_phone'))
            ]);
        }
    }

    protected function orderSource()
    {
        if($this->request->has('source')){
            $source = $this->request->get('source');

            switch ($source) {
                case (1):
                    $source = Order::SOURCE_PROGRAM;
                    break;
                case (2):
                    $source = Order::SOURCE_SCHEDULE;
                    break;
                case (3):
                    $source = Order::SOURCE_LANDING;
                    break;
            }

            $this->merge([
                'source' => $source
            ]);
        }
    }

    protected function formatSum()
    {
        if($this->request->has('sum')){
            $this->merge([
                'sum' => $this->request->get('sum') * 100
            ]);
        }
    }

    protected function checkIsSale()
    {
        $sale = 0;

        if($this->request->has('is_sale')){
            $is_sale = $this->request->get('is_sale');
            if($is_sale == 1) {
                $sale = 1;
            }
        }

        $this->merge([
            'is_sale' => $sale
        ]);
    }

    protected function orderDescription()
    {
        $description = 'Оплата мастер-класса';

        if($this->request->has('description')){
            $description = $this->request->get('description');
        }

        $this->merge([
            'description' => $description
        ]);
    }
}